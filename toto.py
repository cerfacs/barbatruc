"""Example on how to solve a pollution problem with the navier stokes solver"""

import numpy as np

from barbatruc.fluid_domain import DomainRectFluid
from barbatruc.fd_ns_2d import NS_fd_2D_explicit
from barbatruc.lattice import Lattice

__all__ = ["cfd_poiseuille"]


def cfd_poiseuille(nsave):
    """Startup computation
    solve a poiseuille problem
    """

    width = 0.3
    length = width*4
    vel = 1.
    t_end = 1.*length/vel

    dom = DomainRectFluid(dimx=length, dimy=width, delta_x=width/30, nu_=0.001)
    dom.switch_bc_xmin_inlet(vel_u=vel)
    dom.switch_bc_xmax_outlet()
    dom.switch_bc_ymax_wall_noslip()
    dom.switch_bc_ymin_wall_noslip()
    dom.fields["vel_u"] += vel
    
    print(dom)

    time = 0.0
    time_step = t_end/nsave

    #solver = Lattice(dom, max_vel=2*vel)
    solver = NS_fd_2D_explicit(dom, max_vel=2*vel)
    for i in range(nsave):
        time += time_step
        solver.iteration(time, time_step)
        print("\n\n===============================")
        print(f"\n  Iteration {i+1}, Time :, {time}s")
        print(f"  Reynolds : {dom.reynolds(width)}")
        print(dom.fields_stats())
        dom.dump(time=time)
    #solver.plot_monitor()
    #dom.show_fields()
    #dom.show_flow()
    dom.show_raw()

    #dom.show_profile_y()

    print('Normal end of execution.')


if __name__ == "__main__":
    cfd_poiseuille(10)
