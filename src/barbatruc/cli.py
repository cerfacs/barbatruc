#!/usr/bin/env python
"""
cli.py

Command line interface for tools in tekigo
"""

import click
import pkg_resources   # import resource_stream
import webbrowser


@click.group()
def main_cli():
    """---------------   BARBATRUC  --------------------

You are now using the Command line interface of BARBATRUC,
a Python3 training package on CFD ,
created at CERFACS (https://cerfacs.fr).

This is a python package currently installed in your python environement.
"""
    pass


@click.command()
@click.argument('name', nargs=1)
@click.argument(
    'template',
    type=click.Choice(
        [
            'cfd_poiseuille',
            'cfd_lid_driven',
            'cfd_cylinder',
        ],
        case_sensitive=False),
    default='poiseuille',
    )
def create(name, template):
    """Wizard creating a BARBATRUC script NAME.py.

    The NAME is a string the name of your new project.
    The TEMPLATE is one of the following templates:

    * cfd_poiseuille : the 2D poiseuille flow

    * cfd_lid_driven : the lid driven cavity

    * cfd_cylinder : the karman street

    """
    with pkg_resources.resource_stream(
            __name__, f"examples/{template}.py") as fin:
        tmp = fin.read().decode('utf8')

    namefile = f"./{name}.py"
    print(f"Generating dummy inputfile {namefile} for BARBATRUC")
    with open(namefile, "w+") as fout:
        fout.write(tmp)

main_cli.add_command(create)


@click.command()
@click.argument(
    'view',
    type=click.Choice(
        [
            'glob',
            'solver',
        ],
        case_sensitive=False),
    default='glob',
    )
def monitor(view):
    """Monitor a job.

    The VIEW id selectig what to monitor 

    * glob : barbadata_glob, global fields

    * solver : barbadata_solver, solver behavior

    """
    from pkg_resources import resource_string
    import yaml
    from arnica.utils.showy import showy

    if view == "glob":
        datafile = "barbadata_glob.yml"
        layout_string = resource_string('barbatruc', ("showy_glob.yml"))
        showy_layout = yaml.safe_load(layout_string)
    elif view == "solver":
        datafile = "barbadata_solver.yml"
        layout_string = resource_string('barbatruc', ("showy_solver.yml"))
        showy_layout = yaml.safe_load(layout_string)
    else:
        raise RuntimeError("No corresponding view")

    with open(datafile, "r") as fin:
        data_read = yaml.safe_load(fin)
    data = dict()
    cols = sorted(data_read[0].keys())
    for item in cols:
        data[item] = list()
    for rec in data_read:
        for item in cols:
            data[item].append(rec[item]) 

    showy(showy_layout, data)
   
main_cli.add_command(monitor)

@click.command()
def docu():
    """Open Barbatruc documentation
    """
    webbrowser.open_new("https://arnica.readthedocs.io/en/latest/")
main_cli.add_command(docu)
