"""Example on how to solve a Karmann Street problem with the navier stokes solver"""

from barbatruc.fluid_domain import DomainRectFluid
from barbatruc.fd_ns_2d import NS_fd_2D_explicit
from barbatruc.lattice import Lattice

# from barbatruc.lattice import Lattice

__all__ = ["cfd_cylinder"]


# pylint: disable=duplicate-code
def cfd_cylinder(nsave):
    """Startup computation
    solve a cylinder obstacle problem
    """
    lenght = 1.0
    vel = 1.
    diameter = 0.05
    t_end = 4.0 * lenght / vel
    dom = DomainRectFluid(
        dimx=lenght,
        dimy=0.5 * lenght,
        delta_x=diameter/10, 
        nu_=0.0005)
    dom.add_obstacle_circle(x_c=0.2 * lenght, radius=0.5 * diameter)
    dom.switch_bc_xmin_inlet(vel_u=vel)
    dom.switch_bc_xmax_outlet()
    dom.fields["vel_u"] += vel
    dom.fields["vel_v"] += 0.01 * vel

    print(dom)
    time_step = t_end / nsave

    # solver = Lattice(
    #     dom,pp_info=1,
    #     verbose=True,
    # )
    solver = NS_fd_2D_explicit(
        dom,
        #obs_ib_integral=True, obs_ib_factor=6.e3,
        obs_ib_factor=1.e5,
        damp_outlet=True,
        #verbose=True,
    )

    for i in range(nsave):
        solver.time_advance(time_step)
        print("\n\n===============================")
        print(f"\n  Iteration {i+1}/{nsave}, Time :, {dom.time}s")
        print(f"  Reynolds : {dom.reynolds(diameter)}")
        print(f"  Mach : {dom.mach()}")
        print(dom.fields_stats())
        dom.dump_paraview()
        dom.dump_global()
    dom.show_fields()
    dom.show_flow()
    dom.show_profile_y(xtgt=0.5)

    print("Normal end of execution.")

if __name__ == "__main__":
    cfd_cylinder(200)
