"""

Data sturructure:
==================

The data structure of barbatruc is a regular,
 rectilinear grid with a constant spacing `delta_x`.
This structure is chosen to be compatible with both Finite Differences
 Navier-Stokes and Lattice Boltzmann solvers.
"""
import os
import shutil
import math
import h5py
import numpy as np

# from scipy.ndimage import gaussian_filter
import matplotlib.pyplot as plt
from PIL import Image
import yaml
import hdfdict as h5d


from barbatruc.fd_operators import grad_2, scnd_der_2
from arnica.utils import NpArray2Xmf, create_time_collection_xmf

AUTHORIZED_SIZE = 260000
TRACKED_FIELDS = ["vel_u", "vel_v", "scal", "press", "ib_force_x", "ib_force_y"]
GLOBAL_MONITOR_FILE = "barbadata_glob.yml"


RGAS = 287.15
GAMMA = 1.4

__all__ = ["DomainRectFluid", "domain_from_image", "blur"]


# pylint: enable=too-many-instance-attributes,too-many-public-methods
class DomainRectFluid:
    """Fluid Domain Class"""

    # pylint: disable=too-many-arguments
    def __init__(self, dimx=1.0, dimy=0.5, delta_x=0.02, rho=1.16, temp=300., nu_=1.872e-5, time = 0):
        """Intialize the domain

        The geometry is given with DIMX (horizontal)
        and DIMY vertical dimensions.
        The nuber of grid nodes is deduced with DELTA_X.
        The lower left corner of the domain is always at (0,0).

        By default, X and Y oundary conditions are set to periodic.
        :param dimx: float, x size [m]
        :param dimy: float, y size [m]
        :param delta_x: spatial discretization, y size [m]
        :param rho: density [Kg/m3]
        :param nu: viscosity [Kg/m/s]
        """
        n_nodes_x = round(dimx / delta_x)+1
        n_nodes_y = round(dimy / delta_x)+1

        size = n_nodes_x * n_nodes_y
        if size > AUTHORIZED_SIZE:
            raise RuntimeError(f"size too big: {n_nodes_x} x {n_nodes_y}")

        self.dims = ((n_nodes_y-1) * delta_x, (n_nodes_x-1) * delta_x)
        self.shape = (n_nodes_y, n_nodes_x)
        self.delta_x = delta_x
        self.x_range = np.linspace(0.0, dimx, num=n_nodes_x)
        self.y_range = np.linspace(0.0, dimy, num=n_nodes_y)
        self.x_coor, self.y_coor = np.meshgrid(self.x_range, self.y_range)
        self.time = time

        # All fields
        self.fields = dict()
        for field in TRACKED_FIELDS:
            self.fields[field] = np.zeros(self.shape)
        self.obstacle = np.zeros(self.shape, dtype=np.bool)
        
        # For LBM storage
        self.streamf = None

        # Ref state is the global state.
        self.Tref = temp
        self.nu_ref = nu_
        self.rho_ref = rho
        self.p_ref = self.rho_ref*RGAS*self.Tref
        
        self.fields["press"] = np.full(self.shape, self.p_ref)
        

        self.csound = np.sqrt(GAMMA * RGAS * self.Tref)

        self.sponge = np.zeros(self.shape)
        self.saved_xmf = list()

        self.set_source_terms()
        self.switch_bc_x_perio()
        self.switch_bc_y_perio()

        if os.path.exists(GLOBAL_MONITOR_FILE):
            os.remove(GLOBAL_MONITOR_FILE)
        with open(GLOBAL_MONITOR_FILE, "w") as fout:
            fout.write("# Barbatruc monitoring of global fields\n\n")

    # pylint: disable=line-too-long
    def __str__(self):
        """Define what print(domain) will return"""

        out = ".  Rectangular Grid.    "
        out += "\n=======================\n"
        out += f"\n     y_max: {self.dims[0]}m, nodes: {self.shape[0]}, dx: {self.delta_x}"
        out += f"\n     +----------------------------+"
        out += f"\n     |                            |"
        out += f"\n     |                            |"
        out += f"\n     |                            |"
        out += f"\n     |                            |"
        out += f"\n     |                            |"
        out += f"\n     +----------------------------+"
        out += f"\n   (0,0)                        x_max: {self.dims[1]}m, nodes: {self.shape[1]}, dx: {self.delta_x}"
        out += f"\n\n"
        out += f"\n                 {self.bc_ymax_type}      "
        out += f"\n     +----------------------------+"
        out += f"\n     |                            |"
        out += f"\n   {self.bc_xmin_type}               "
        out += f"\n     |                          {self.bc_xmax_type}"
        out += f"\n     |                            |"
        out += f"\n     |                            |"
        out += f"\n     +----------------------------+"
        out += f"\n                  {self.bc_ymin_type}      "
        out += f"\n"
        out += self.fields_stats()
        out += f"\n"
        return out

    def set_source_terms(self, source_terms=None, time_dependent=False):
        """Set up source terms

        :param source_terms: dict of nparays, shape equalt to dom.shape
            dict is {force_x: , force_y: , scal:}
        :param time_dependent: boolean,
            specify that source_terms are tuime dependanet functions
        """
        if source_terms is None:
            self.source_terms = None
            self.time_dependent_sources = False
        else:
            self.source_terms = source_terms
            self.time_dependent_sources = time_dependent

    def dump_global(self):
        """Append global monitoring of fields."""

        data = dict()
        data["time"] = self.time
        # Maw
        for item in ["vel_u", "vel_v", "scal", "press"]:
            data[item + "_avg"] = float(self.fields[item].mean())
            data[item + "_min"] = float(self.fields[item].min())
            data[item + "_max"] = float(self.fields[item].max())

        force_x = (
            float(np.sum(self.fields["ib_force_x"])) * self.rho_ref * self.delta_x ** 2
        )
        force_y = (
            float(np.sum(self.fields["ib_force_y"])) * self.rho_ref * self.delta_x ** 2
        )
        data["ib_force_x"] = force_x
        data["ib_force_y"] = force_y
        max_vel = max(1e-8, float(self.velocity().max()))

        obs_ratio = np.average(self.obstacle * 1.0)
        if obs_ratio > 0.0:
            obs_vel_u = self.obstacle * self.fields["vel_u"] / obs_ratio
            obs_vel_v = self.obstacle * self.fields["vel_v"] / obs_ratio
            data["obs_vel_u"] = float(obs_vel_u.mean()) / max_vel
            data["obs_vel_v"] = float(obs_vel_v.mean()) / max_vel
        else:
            data["obs_vel_u"] = 0.0
            data["obs_vel_v"] = 0.0

        vort, div = self.vort_div()
        data["vort_avg"] = float(vort.mean()) / max_vel * self.delta_x
        data["vort_max"] = float(vort.max()) / max_vel * self.delta_x
        data["vort_min"] = float(vort.min()) / max_vel * self.delta_x
        data["div_avg"] = float(div.mean()) / max_vel * self.delta_x
        data["div_max"] = float(div.max()) / max_vel * self.delta_x
        data["div_min"] = float(div.min()) / max_vel * self.delta_x

        list_ = list()
        list_.append(data)
        with open(GLOBAL_MONITOR_FILE, "a") as fout:
            yaml.dump(list_, fout, indent=4)

    def fields_stats(self):
        """ Return field statistics report as a string."""
        out = "\n.  Fields stats    "
        out += "\n======================="
        out += "\n.  min|avg|max"

        for field in ["vel_u", "vel_v", "scal", "press"]:
            out += f"\n {field} :"
            out += f" {self.fields[field].min()} |"
            out += f" {self.fields[field].mean()} |"
            out += f" {self.fields[field].max()} "
        out += "\n"
        return out

    # pylint: disable=attribute-defined-outside-init
    def switch_bc_x_perio(self):
        """ set to a x-periodic domain."""
        self.bc_xmax_type = "periodic"
        self.bc_xmax_values = dict()
        self.bc_xmin_type = "periodic"
        self.bc_xmin_values = dict()

    # pylint: disable=attribute-defined-outside-init
    def switch_bc_y_perio(self):
        """ set to a y-periodic domain."""
        self.bc_ymax_type = "periodic"
        self.bc_ymax_values = dict()
        self.bc_ymin_type = "periodic"
        self.bc_ymin_values = dict()

    def switch_bc_xmin_inlet(self, vel_u=1.0):
        """ set to a dirichlet of velocity VEL_U"""
        self.bc_xmin_type = "inlet"
        self.bc_xmin_values = {"vel_u": vel_u, "vel_v": 0.0}

    def switch_bc_ymax_moving_wall(self, vel_u=1.0):
        """ set to a dirichlet of velocity VEL_U"""
        self.bc_ymax_type = "moving_wall"
        self.bc_ymax_values = {"vel_u": vel_u, "vel_v": 0.0}

    def switch_bc_xmin_wall_noslip(self):
        """ set to a dirichlet BC: vel_u, vel_v null."""
        self.bc_xmin_type = "wall_noslip"
        self.bc_xmin_values = dict()

    def switch_bc_xmax_wall_noslip(self):
        """ set to a dirichlet BC: vel_u, vel_v null."""
        self.bc_xmax_type = "wall_noslip"
        self.bc_xmax_values = dict()

    def switch_bc_xmax_outlet(self):
        """ set to an outlet"""
        self.bc_xmax_type = "outlet"
        self.bc_xmax_values = dict()

    def switch_bc_ymin_wall_noslip(self):
        """ set to a dirichlet BC, vel_u, vel_v null."""
        self.bc_ymin_type = "wall_noslip"
        self.bc_ymin_values = dict()

    def switch_bc_ymax_wall_noslip(self):
        """ set to a dirichlet BC . vel_u, vel_v null."""
        self.bc_ymax_type = "wall_noslip"
        self.bc_ymax_values = dict()

    def switch_bc_ymin_wall_slip(self):
        """ set to a dirichlet BC.  vel_v. null"""
        self.bc_ymin_type = "wall_slip"
        self.bc_ymin_values = dict()
    
    def switch_bc_ymax_wall_slip(self):
        """ set to a dirichlet BC. vel_v. null"""
        self.bc_ymax_type = "wall_slip"
        self.bc_ymax_values = dict()

    def switch_bc_ymax_symmetry(self):
        """ set to a dirichlet BC. vel_v. null"""
        self.bc_ymax_type = "symmetry"
        self.bc_ymax_values = dict()

    def perio_xy(self):
        """Return x and y Periodicities booleans."""
        periox = False
        perioy = False
        if self.bc_xmax_type == "periodic" and self.bc_xmin_type == "periodic":
            periox = True
        if self.bc_ymax_type == "periodic" and self.bc_ymin_type == "periodic":
            perioy = True

        return periox, perioy

    def add_obstacle_circle(self, x_c=None, y_c=None, radius=None):
        """Add cicular obstacle.

        :param x_c: x center in m
        :param y_c: y center in m
        :pram radius: radius in meters
        """
        if x_c is None:
            x_c = 0.33 * self.dims[1]
        if y_c is None:
            y_c = 0.5 * self.dims[0]
        if radius is None:
            radius = 0.1 * self.dims[0]
        radius_field = np.hypot(self.x_coor - x_c, self.y_coor - y_c)
        self.obstacle = np.where(radius_field < radius, True, self.obstacle)

    def add_obstacle_rect(self, ll_x=None, ll_y=None, ur_x=None, ur_y=None):
        """Add rectangular obstacle.

        if one limit is not given, takes the most extreme bound.

        :param ll_x:lower left x
        :param ll_y: lower left y
        :param ll_x: upper right x
        :param ll_y: upper right y
        """
        if ll_x is None:
            ll_x = 0
        if ll_y is None:
            ll_y = 0
        if ur_x is None:
            ur_x = self.dims[1]
        if ur_y is None:
            ur_y = self.dims[0]

        rect_field = np.ones(self.shape)
        rect_field = np.where(self.x_coor < ll_x, 0.0, rect_field)
        rect_field = np.where(self.x_coor > ur_x, 0.0, rect_field)
        rect_field = np.where(self.y_coor < ll_y, 0.0, rect_field)
        rect_field = np.where(self.y_coor > ur_y, 0.0, rect_field)

        self.obstacle = np.maximum(rect_field, self.obstacle)

    def nullify_on_obstacle(self, field, where="inside"):
        """Nullify a field where the obstacle is defined."""
        obst = self.obstacle

        if isinstance(field, np.ndarray):
            if len(field.shape) == 3:
                obst = self.obstacle[:, :, np.newaxis]

        def find_edges(mask, axis):
            mp1 = np.roll(mask, 1, axis=axis)
            mm1 = np.roll(mask, -1, axis=axis)
            edges = np.where(mp1 * mask == True, 1.0, 0.0) + np.where(
                mm1 * mask == True, 1.0, 0.0
            )
            return edges

        edges_x = find_edges(obst, axis=1)
        edges_x += find_edges(edges_x, axis=0)
        edges_y = find_edges(obst, axis=0)
        edges_y += find_edges(edges_y, axis=1)

        out = field
        if where == "inside":
            out = np.where(obst == True, 0.0, out)
        if where in ["edges_x", "edges"]:
            out = np.where(edges_x > 0.0, 0.0, out)
        if where in ["edges_y", "edges"]:
            out = np.where(edges_y > 0.0, 0.0, out)

        return out

    def show_fields(self):
        """Show the 4 fields in a 4 graph panel."""
        x_linp1 = np.linspace(0, self.dims[1], self.shape[1] + 1)
        y_linp1 = np.linspace(0, self.dims[0], self.shape[0] + 1)
        max_vel = max(
            self.fields["vel_u"].max(),
            self.fields["vel_v"].max(),
            -self.fields["vel_u"].min(),
            -self.fields["vel_v"].min(),
        )

        fig = plt.figure()
        ax1 = fig.add_subplot(221)
        ax1.set_title("velu")
        plt.pcolormesh(
            x_linp1,
            y_linp1,
            self.fields["vel_u"],
            vmin=-max_vel,
            vmax=max_vel,
            cmap="bwr",
        )
        plt.colorbar()
        ax1.set_aspect("equal")
        ax2 = fig.add_subplot(222)
        ax2.set_title("velv")
        ax2.set_aspect("equal")
        plt.pcolormesh(
            x_linp1,
            y_linp1,
            self.fields["vel_v"],
            vmin=-max_vel,
            vmax=max_vel,
            cmap="bwr",
        )
        ax3 = fig.add_subplot(223)
        ax3.set_title("press")
        ax3.set_aspect("equal")
        plt.pcolormesh(x_linp1, y_linp1, self.fields["press"], cmap="Greys")
        plt.colorbar()

        ax4 = fig.add_subplot(224)
        ax4.set_title("scal")
        ax4.set_aspect("equal")
        plt.pcolormesh(x_linp1, y_linp1, self.fields["scal"], cmap="YlGn")
        plt.colorbar()
        plt.show()

    def fields_at_pos(self, x_coor, y_coor):
        """Velocity ad position x, y"""
        ix_r = x_coor / self.delta_x
        ix_ = math.floor(ix_r)
        r_x = ix_r - ix_
        iy_r = y_coor / self.delta_x
        iy_ = math.floor(iy_r)
        r_y = iy_r - iy_

        f_out = dict()
        for field in TRACKED_FIELDS:
            ix_iy = self.fields[field][iy_, ix_]
            ixp1_iy = self.fields[field][iy_, ix_+1]
            ix_iyp1 = self.fields[field][iy_+1, ix_]
            ixp1_iyp1 = self.fields[field][iy_+1, ix_+1]
            if len(self.fields[field].shape) == 3:
                ix_iy = self.fields[field][iy_, ix_, :]
                ixp1_iy = self.fields[field][iy_, ix_+1, :]
                ix_iyp1 = self.fields[field][iy_+1, ix_, :]
                ixp1_iyp1 = self.fields[field][iy_+1, ix_+1, :]

            f_y = (
                (1.0 - r_x) * ix_iy
                + r_x * ixp1_iy
            )
            f_yp1 = (
                (1.0 - r_x) * ix_iyp1
                + r_x * ixp1_iyp1
            )
            f_out[field] =  (1.0 - r_y) * f_y + r_y * f_yp1

        return f_out

    def show_flow(self):
        """Show the flow with magnitude and streamlines.

        Spawn Matplotlib blocking window
        """
        magv = np.hypot(self.fields["vel_u"], self.fields["vel_v"])

        x_lin = np.linspace(0, self.dims[1], self.shape[1])
        y_lin = np.linspace(0, self.dims[0], self.shape[0])
        x_linp1 = np.linspace(0, self.dims[1], self.shape[1] + 1)
        y_linp1 = np.linspace(0, self.dims[0], self.shape[0] + 1)

        fig = plt.figure()
        ax1 = fig.add_subplot(111)
        ax1.set_aspect("equal")
        plt.pcolormesh(x_linp1, y_linp1, magv, cmap="jet")
        plt.colorbar()
        plt.streamplot(
             x_lin,y_lin, self.fields["vel_u"], self.fields["vel_v"], density=[0.5, 1]
        )
        plt.show()

    def show_profile_y(self, xtgt=None):
        """Show a profile at X=XTGT

        Show profile at xmax if XTGT is not provided.

        :param xtgt: float, x position.

        Spawn Matplotlib blocking window
        """
        if xtgt is None:
            p_index = -1
        else:
            p_index = int(xtgt / self.dims[1] * (self.shape[1] - 1))

        plt.plot(
            self.y_coor[:, p_index],
            self.fields["vel_u"][:, p_index],
            label="vel_u",
            marker="o",
            color="blue",
        )
        plt.plot(
            self.y_coor[:, p_index],
            self.fields["vel_v"][:, p_index],
            label="vel_v",
            marker="X",
            color="green",
        )
        plt.ylabel("velocity (m/s)")
        plt.xlabel("y-position (m)")
        plt.legend()
        plt.show()

    def show_profile_x(self, ytgt=None):
        """Show a profile at Y=XTGT

        Show profile at xmax if XTGT is not provided.

        :param ytgt: float, y position.

        Spawn Matplotlib blocking window
        """
        if ytgt is None:
            p_index = -1
        else:
            p_index = int(ytgt / self.dims[0] * (self.shape[0] - 1))

        plt.plot(
            self.x_coor[p_index, :],
            self.fields["vel_u"][p_index, :],
            label="vel_u",
            marker="o",
            color="blue",
        )
        plt.plot(
            self.x_coor[p_index, :],
            self.fields["vel_v"][p_index, :],
            label="vel_v",
            marker="X",
            color="green",
        )
        plt.ylabel("velocity (m/s)")
        plt.xlabel("x-position (m)")
        plt.legend()
        plt.show()

    def show_raw(self):
        """Show the flow magnitude as a matrix."""

        plt.matshow(self.velocity())
        plt.colorbar()
        plt.show()

    def show_debit_over_x(self):
        """Show the flow magnitude as a matrix."""

        debit = np.sum(self.fields["vel_u"], axis=0) * self.delta_x * self.rho_ref
        plt.plot(
            self.x_coor[0, :],
            debit,
            # label="Massflow Kg/s",
            marker="X",
            color="green",
        )
        plt.ylim(0,)
        plt.xlabel("y-position (m)")
        plt.ylabel("Massflow Kg/s")
        # plt.legend()
        plt.show()

    def reynolds(self, lenght):
        """ Return reynolds number """
        vel = np.max(self.velocity())
        reynolds = self.rho_ref * vel / self.nu_ref * lenght
        return reynolds

    def mach(self,):
        """ Return reynolds number """
        vel = np.max(self.velocity())
        mach =  vel / self.csound
        return mach
    

    def velocity(self):
        """ Return rvelocity field """
        out = np.hypot(self.fields["vel_u"], self.fields["vel_v"])
        return out

    def vort_div(self):
        """ Return rvelocity field """
        self.periox, self.perioy = self.perio_xy()
        gux, guy = grad_2(
            self.fields["vel_u"], self.delta_x, periox=self.periox, perioy=self.perioy,
        )
        gvx, gvy = grad_2(
            self.fields["vel_v"], self.delta_x, periox=self.periox, perioy=self.perioy,
        )
        vort = guy - gvx
        div = gux + gvy
        return vort, div

    def dump_paraview(self, name="barbaresult", folder="results_paraview"):
        """Dump the result into xdmf format"""
        if not os.path.isdir(folder):
            os.mkdir(folder)
        fname = f"{name}_{len(self.saved_xmf):03}.xmf"
        n2x = NpArray2Xmf(os.path.join(folder, fname), time=self.time)
        n2x.create_grid(self.x_coor, self.y_coor, np.zeros_like(self.x_coor))
        for field in ["vel_u", "vel_v", "press", "ib_force_x", "ib_force_y"]:
            n2x.add_field(self.fields[field], field)
        vort, div = self.vort_div()

        if len(self.fields["scal"].shape) == 3:
            for i in range(self.fields["scal"].shape[-1]):
                 n2x.add_field(self.fields["scal"][:, :, i], "scal_"+str(i))
        else: 
            n2x.add_field(self.fields["scal"], "scal")
        n2x.add_field(vort, "vorticity")
        n2x.add_field(div, "divergence")
        n2x.add_field(1.0 * self.obstacle, "obstacle")
        n2x.add_field(self.sponge, "sponge")
        n2x.add_field(self.velocity(), "velocity")
        # n2x.add_field(self.smago(), "smago")
        n2x.dump()
        self.saved_xmf.append(f"{fname}")

        # do not work yet...
        create_time_collection_xmf(
            self.saved_xmf, os.path.join(folder, "barbaresult.xmf")
        )

    def load_flow(self, solpath):
        """ Extracts specific fields from existing barbatruc solution
            and writes into another file

            solution_file:
            ==========
            solution_file: h5 file composed of the coordinates and variables

            Returns:
            =======
            coord : coordinate - x, y, z
            sol : variables
        """
        with h5py.File(solpath, "r") as sol:
            solution = h5d.load(sol, lazy=False)
        for group in solution:
            if group == "variables":
                # Vorticity is excluded
                varnames = [
                    "scal",
                    "vel_u",
                    "vel_v",
                    "press",
                    "ib_force_x",
                    "ib_force_y",
                ]
                for var in varnames:
                    self.fields[var] = solution[group][var]


def domain_from_image(fname, pix_size, obs_color=(0, 0, 0), rho=1.12, nu_=1.0):
    """Create a domain from an image"""

    img = Image.open(fname)
    (width, height) = img.size
    red_channel = np.array(img.getdata(band=0))
    green_channel = np.array(img.getdata(band=1))
    blue_channel = np.array(img.getdata(band=2))

    width_m = width * pix_size
    height_m = height * pix_size

    out = DomainRectFluid(
        dimx=width_m, dimy=height_m, delta_x=pix_size, rho=rho, nu_=nu_
    )

    obs = 1.0 * (
        (red_channel == obs_color[0])
        * (green_channel == obs_color[1])
        * (blue_channel == obs_color[2])
    ).reshape((height, width))

    out.obstacle = np.flip(obs, 0)

    return out


def blur(arr_):
    """Apply a gather scater smothing to an array.

    Deprecated, use scipy.image gaussian filter...
    """
    kernel = np.array([[1.0, 2.0, 1.0], [2.0, 4.0, 2.0], [1.0, 2.0, 1.0]])
    kernel = kernel / np.sum(kernel)
    arraylist = []
    for y_i in range(3):
        temparray = np.copy(arr_)
        temparray = np.roll(temparray, y_i - 1, axis=0)
        for x_i in range(3):
            temparray_x = np.copy(temparray)
            temparray_x = np.roll(temparray_x, x_i - 1, axis=1) * kernel[y_i, x_i]
            arraylist.append(temparray_x)
    arraylist = np.array(arraylist)
    arraylist_sum = np.sum(arraylist, axis=0)
    return arraylist_sum
