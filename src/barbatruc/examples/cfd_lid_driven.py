"""Example on how to solve a Lid driven cavity problem with the navier stokes solver"""

from barbatruc.fluid_domain import DomainRectFluid
from barbatruc.fd_ns_2d import NS_fd_2D_explicit

# from barbatruc.lattice import Lattice

__all__ = ["cfd_lid_driven"]


# pylint: disable=duplicate-code
def cfd_lid_driven(nsave, tchar):
    """Startup computation
    solve a lid_driven_cavity  problem
    """
    size = 1.0
    dom = DomainRectFluid(dimx=size, dimy=size, delta_x=size / 60, rho=1.0, nu_=0.01)

    vel = 1.0
    t_end = tchar * size / vel
    dom.switch_bc_xmin_wall_noslip()
    dom.switch_bc_xmax_wall_noslip()
    dom.switch_bc_ymax_moving_wall(vel_u=vel)
    dom.switch_bc_ymin_wall_noslip()

    time = 0.0
    time_step = t_end / nsave

    print(dom)
    # solver = Lattice(dom, max_vel=2*vel)
    solver = NS_fd_2D_explicit(
        dom, obs_ib_factor=0.01, press_maxsteps=200, press_tol=5.0e-3,
    )
    for i in range(nsave):
        time += time_step
        solver.time_advance(time_step)
        print("\n\n===============================")
        print(f"\n  Iteration {i+1}/{nsave}, Time :, {time}s")
        print(f"  Reynolds : {dom.reynolds(size)}")
        print(dom.fields_stats())
        dom.dump_paraview()
        dom.dump_global()
    dom.show_fields()
    dom.show_flow()
    dom.show_profile_y(xtgt=0.41)
    dom.show_profile_x(ytgt=0.41)
    print("Normal end of execution.")


if __name__ == "__main__":
    cfd_lid_driven(10, 10)
