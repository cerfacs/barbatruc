#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages
from glob import glob
from os.path import basename

with open('README.md') as f:
    readme = f.read()

with open('LICENCE.md') as f:
    license = f.read()

setup(
    name='barbatruc',
    version='0.2.0a',
    description='Training computational fluid dynamics solver',
    keywords=["education", "CFD", "Navier-Stokes", "LBM"],
    long_description=readme,
    long_description_content_type="text/markdown",
    author='COOP',
    author_email='coop@cerfacs.fr',
    url='https://nitrox.cerfacs.fr/open-source/barbatruc',
    license=license,
    packages=find_packages("src"),
    package_dir={"": "src"},
    py_modules=[path.splitext(basename(path))[0] for path in glob('src/*.py')],
    include_package_data=True,
    entry_points={
        "console_scripts": [
            "barbatruc = barbatruc.cli:main_cli",
        ]
    },
    setup_requires=['pytest-runner'],
    tests_require=['pytest', 'pytest-cov'],
    install_requires=[
        "arnica",
        "numpy",
        "h5py",
        "click",
        "Pillow",
        "scipy",
        "matplotlib",
        "hdfdict"
    ],
)

