"""Tracer factory for barbatruc"""

import numpy as np
from scipy.interpolate import interp1d

EPS = 1e-6
OUT = np.array([0.0, 0.0])

__all__ = ["Agents"]


class Agents:
    """Group of tracers"""

    def __init__(self, dom, time_list, list_positions, rad=0.5, c_d=0.9):
        """Startup.

        :param dom: a barbatruc domain.
        :param time_list: a numpy array of instants [s] (shape (m)).
        :param list_position: a list of numpy array of position x,y [s] (shape (m,2)).
        :param rad: float, radius of tracer.
        """
        self.dom = dom


        self.rad = rad
        self.c_d = c_d
        self.family = list()

        self.n_agents = len(list_positions)
        
        for agent_pos in list_positions:
            self.family.append(SingleAgent(time_list, agent_pos))

        # reconfigure scalrs to be of shape 3
        self.shape_agents = (
            self.dom.shape[0], self.dom.shape[1], self.n_agents)
        self.dom.fields["scal"] = np.zeros((self.shape_agents))
        self.cumsum_agent = np.zeros((self.n_agents, self.n_agents))


    def source_terms(self, time):
        """return source terms on domain a TIME in s.

        :returns:
            dict of numpy arrays compatible with domain
            {"scal": , "force_x": , "force_y": }
        """

        aspot = np.zeros(self.shape_agents)
        aforce_x = np.zeros(self.dom.shape)
        aforce_y = np.zeros(self.dom.shape)
        for i, agent in enumerate(self.family):
            if agent.exists(time):
                (x_pos, y_pos) = agent.path(time)
                rad = np.hypot(self.dom.x_coor - x_pos, self.dom.y_coor - y_pos)
                smooth = 0.5 * self.rad
                spot = 0.5 - 0.5 * np.tanh((rad - self.rad) / smooth)
                spot = np.where(spot > 1e-4, spot, 0.0)
                integ = np.sum(spot) * self.dom.delta_x ** 2
                spot /= integ
                vel = agent.vel(time)
                vals_at_pos = self.dom.fields_at_pos(x_pos, y_pos)
                force_x = spot * (vel[0] - vals_at_pos["vel_u"]) * self.c_d
                force_y = spot * (vel[1] - vals_at_pos["vel_v"]) * self.c_d
                aspot[:, :, i] += spot
                aforce_x += force_x
                aforce_y += force_y
        return {"scal": aspot, "force_x": aforce_x, "force_y": aforce_y}

    def add_expositions(self, time, delta_t):
        for i, agent in enumerate(self.family):
           if agent.exists(time):
                (x_pos, y_pos) = agent.path(time) 
                vals_at_pos = self.dom.fields_at_pos(x_pos, y_pos)
                self.cumsum_agent[:, i] += vals_at_pos["scal"]*delta_t
    
    def dump_expositions(self, fname):
        np.savetxt(fname, self.cumsum_agent, header="Muxu exposition output")
    

class SingleAgent:
    """A single agent moving in a domain."""

    def __init__(self, time_list, position):
        """Startup.

        :param time_list: a numpy array of instants [s] (shape (m))
        :param position: a numpy array of position x,y [s] (shape (m,2))
        """
        self.tmin = 0.0
        self.tmax = 0.0

        active_time_list = list()
        for i, time in enumerate(time_list):
            pos = position[i, :]
            if np.array_equal(pos, OUT):
                pass
            else:
                active_time_list.append(time)
        if active_time_list:
            self.tmin = active_time_list[0]
            self.tmax = active_time_list[-1]

        self.path = interp1d(
            time_list,
            position,
            kind="linear",
            axis=0,
            bounds_error=False,
            fill_value=OUT,
        )

    def exists(self, time):
        """Return true is tracer exists at TIME"""
        if time >= self.tmin and time <= self.tmax:
            out = True
        else:
            out = False
        return out

    def pos(self, time):
        """Return position [x, y] at TIME"""
        return self.path(time)

    def vel(self, time):
        """Return veolcity [u_x, u_y] at TIME"""
        pos = self.path(time)
        pos_eps = self.path(time - EPS)
        vel = (pos - pos_eps) / EPS

        return vel
