# Changelog

All notable changes to this project will be documented in this file.
The format is based on Keep a Changelog,
and this project adheres to Semantic Versioning.


## [0.1.0] 2019 / 12 / 11

### Added
 - Solvers navier stokes et LBM 2DQ9
 - examples folder

### Changed
 [ - ] 
 
### Fixed
 [ - ] 

### Deprecated
 [ - ] 



