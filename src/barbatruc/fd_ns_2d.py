"""

Incompressible Navier Stokes Solver
===================================

The interactive module 12 steps to Navier-Stokes is one of several
components of the Computational Fluid Dynamics class taught by Professor
Lorena Barba in Boston university between 2009 and 2013.

Dom here after isthe FluidDomain object of barbatruc
"""
import os
import math
from collections import namedtuple
import numpy as np
import matplotlib.pyplot as plt
import yaml
from barbatruc.fd_operators import grad_1, grad_2, scnd_der_2

CFL_MAX = 0.699
FOU_MAX = 0.099
POISSON_MAX_STEPS = 100
SOLVER_MONITOR_FILE = "barbadata_solver.yml"


__all__ = ["NS_fd_2D_explicit"]


# pylint: disable=invalid-name
class NS_fd_2D_explicit:
    """Navier-stockes finite difference incompressible solver."""

    def __init__(
        self,
        dom,
        verbose=False,
        obs_ib_factor=0.01,
        obs_ib_integral=False,
        press_tol=1e-2,
        smago_sgs=False,
        press_maxsteps=100,
        centered_scheme=False,
        damp_outlet=False,
    ):
        """Initialization of the solver.

        :param max_vel: the maximum velocity to be expected (m/s) -CFL bound-
        :param verbose: boolean, mx verbosity in iterations
        :param obs_ib_factor: Immersed boundary gain (1/s) -Heuristic-
        :param obs_ib_integral: boolean, use integral IB id true, proportionnal if False
        :param press_tol: non-dimensional Poisson solver tolerance
        :param press_maxsteps: integer, Poisson solver maximum substeps.
        :param centered_scheme: boolean, switch from 1st order upwind to 2nd order centered.
        :param damp_outlet: boolean, remove extrapolation of pressure for xmax outlet.
        """
        self.dom = dom
        self.verbose = verbose

        self.periox, self.perioy = dom.perio_xy()
        self.obs_ib_factor = obs_ib_factor

        self.obs_ib_integral = 0.0
        if obs_ib_integral:
            self.obs_ib_integral = 1.0

        self.smago_sgs = smago_sgs

        self.log = str()
        
        Controls = namedtuple(
            "Controls",
            [
                "press_maxsteps",
                "press_tol",
                "damp_outlet",
                "centered_scheme",
            ],
        )
        self.ctrl = Controls(
            press_maxsteps, press_tol, damp_outlet, centered_scheme
        )

        self.monitor = dict()
        # self.monitor = {
        #     "time": list(),
        #     "delta_t": list(),
        #     "P_steps": list(),
        #     "P_eps": list(),
        #     "U_mean": list(),
        #     "P_max": list(),
        # }

        if os.path.exists(SOLVER_MONITOR_FILE):
            os.remove(SOLVER_MONITOR_FILE)
        with open(SOLVER_MONITOR_FILE, "w") as fout:
            fout.write("# Barbatruc monitoring of solver qtties\n\n")

    def time_advance(self, duration):
        """Major iteration."""
        self.log = str()
        t_end = self.dom.time + duration
        if self.verbose:
            print("Delta t, Time, max_vel")
        while self.dom.time < t_end:
            
            max_vel_u = np.max(np.abs(self.dom.fields["vel_u"]))
            max_vel_v = np.max(np.abs(self.dom.fields["vel_v"]))
            self.max_vel = max(max_vel_u, max_vel_v, np.finfo(float).eps)
            delta_t = self._mini_delta_t()
            if self.verbose:
                print(f"{delta_t}, {self.dom.time},  {self.max_vel}")
            self.monitor["time"] = self.dom.time
            self.monitor["max_vel_CFL"] = self.max_vel
            self.monitor["max_vel_u"] = float(max_vel_u)
            self.monitor["max_vel_v"] = float(max_vel_v)
            
            self._iteration(delta_t)
            
        if self.verbose:
            print(self.log)

        list_ = list()
        list_.append(self.monitor)
        with open(SOLVER_MONITOR_FILE, "a") as fout:
            yaml.dump(list_, fout, indent=4)

    def _mini_delta_t(self):
        """ Define the mai iteration of the temporal loop.
        """

        delta_t_cfl = CFL_MAX * self.dom.delta_x / self.max_vel
        delta_t_fou = FOU_MAX / self.dom.nu_ref * self.dom.delta_x ** 2
        delta_t = min(delta_t_cfl, delta_t_fou)
        return delta_t

    def _iteration(self, delta_t):
        """Sub iteration of Navier stokes."""
        fields_new = dict()
        self.dom.time += delta_t
        self._apply_bnd_uv()

        
        self._compute_derivatives()
        visco_nu = self._visco()

        fields_new["ib_force_x"] = self.obs_ib_integral * self.dom.fields[
            "ib_force_x"
        ] + (
            -self.obs_ib_factor * self.dom.obstacle *
            self.dom.fields["vel_u"] * delta_t
        )
        fields_new["ib_force_y"] = self.obs_ib_integral * self.dom.fields[
            "ib_force_y"
        ] + (
            -self.obs_ib_factor * self.dom.obstacle *
            self.dom.fields["vel_v"] * delta_t
        )

        p_rhs = self._poisson_rhs(delta_t)
        fields_new["press"], gpx, gpy = self._pressure_poisson(p_rhs)

        source_force_x, source_force_y, source_scal = self._source_terms()

        fields_new["vel_u"] = self.dom.fields["vel_u"] + delta_t * (
            -(
                self.dom.fields["vel_u"] * self.grad_x["vel_u"]
                + self.dom.fields["vel_v"] * self.grad_y["vel_u"]
            )
            - gpx / (2.0 * self.dom.rho_ref)
            + source_force_x
            + self.dom.fields["ib_force_x"]
            + visco_nu * (self.ggrad_x["vel_u"] + self.ggrad_y["vel_u"])
        )

        fields_new["vel_v"] = self.dom.fields["vel_v"] + delta_t * (
            -(
                self.dom.fields["vel_u"] * self.grad_x["vel_v"]
                + self.dom.fields["vel_v"] * self.grad_y["vel_v"]
            )
            - gpy / (2.0 * self.dom.rho_ref)
            + source_force_y
            + self.dom.fields["ib_force_y"]
            + visco_nu * (self.ggrad_x["vel_v"] + self.ggrad_y["vel_v"])
        )

        self._nullify_scal_grads_obs()

        u_conv = self.dom.fields["vel_u"]
        v_conv = self.dom.fields["vel_u"]
        vis_scal = visco_nu
        if len(self.dom.fields["scal"].shape) == 3:
            u_conv = self.dom.fields["vel_u"][:, :, np.newaxis]
            v_conv = self.dom.fields["vel_v"][:, :, np.newaxis]
            vis_scal = visco_nu[:, :, np.newaxis]

        fields_new["scal"] = self.dom.fields["scal"] + delta_t * (
            -(
                u_conv * self.grad_x["scal"]
                + v_conv * self.grad_y["scal"]
            )
            + source_scal
            + vis_scal * (self.ggrad_x["scal"] + self.ggrad_y["scal"])
        )
        self.dom.fields = fields_new

    def _pressure_poisson(self, p_rhs):
        """Define Pressure Poisson iterative function.

        here ggrad_p_x and ggrad_p_y are terms comes
        from the pressure laplacian
        p_rhs is the pressure term coming from div_u

        """
        # p_rhs = self.poisson_rhs(delta_t, grad_x, grad_y)
        press = self.dom.fields["press"].copy()

        p_amp = max(press.max() - press.min(), 1e-8)

        for it_ in range(self.ctrl.press_maxsteps):
            p_old = press.copy()
            ggrad_p_x, ggrad_p_y = scnd_der_2(
                press,
                self.dom.delta_x,
                periox=self.periox,
                perioy=self.perioy,
                no_center=True,
            )

            self._apply_bnd_poisson(ggrad_p_x)

            press = (ggrad_p_x + ggrad_p_y - p_rhs) / \
                4.0 * self.dom.delta_x ** 2

            eps = np.linalg.norm(press - p_old) / p_amp
            if eps < self.ctrl.press_tol:
                self.log += f"\n.  Pressure converged"
                break
            if eps > 1.0e9:
                self.log += f"\n.  vel_u: {np.mean(self.dom.fields['vel_u'])}"
                self.log += f"\n.  pres: {np.mean(press)}"
                self.log += f"\n.  p_old: {np.mean(p_old)}"
                print(self.log)
                raise RuntimeError(f"\n.  Pressure divergence")

        self.log += f"\n.   solved in {it_+1} it."
        self.log += f"\n.   eps: {eps}"

        self.monitor["P_eps"] = float(math.log10(max(eps, 1.0e-8)))
        self.monitor["P_eps_tol"] = float(math.log10(self.ctrl.press_tol))
        self.monitor["P_steps_max"] = self.ctrl.press_maxsteps
        self.monitor["P_steps"] = it_ + 1

        press -= press.mean()
        gpx, gpy = grad_2(
            press, self.dom.delta_x, periox=self.periox, perioy=self.perioy,
        )
        gpx, gpy = self._apply_bnd_press(gpx, gpy)

        return press, gpx, gpy

    def _poisson_rhs(self, delta_t):
        """Compute the presure term from velocity divengence."""
        gux = self.grad_x["vel_u"]
        guy = self.grad_y["vel_u"]
        gvx = self.grad_x["vel_v"]
        gvy = self.grad_y["vel_v"]
        p_rhs = self.dom.rho_ref * (
            (gux + gvy) / delta_t - (gux * gux + 2.0 * guy * gvx + gvy * gvy)
        )
        return p_rhs

    # pylint: disable=attribute-defined-outside-init
    def _compute_derivatives(self):
        """Compute the derivatives of fields."""
        self.grad_x = dict()
        self.grad_y = dict()
        self.ggrad_x = dict()
        self.ggrad_y = dict()
        vel_u_adim = self.dom.fields["vel_u"] / self.max_vel
        vel_v_adim = self.dom.fields["vel_v"] / self.max_vel
        for field in ["vel_u", "vel_v", "scal"]:

            if self.ctrl.centered_scheme:
                self.grad_x[field], self.grad_y[field] = grad_2(
                    self.dom.fields[field],
                    self.dom.delta_x,
                    periox=self.periox,
                    perioy=self.perioy,
                )
            else:
                self.grad_x[field], self.grad_y[field] = grad_1(
                    self.dom.fields[field],
                    self.dom.delta_x,
                    periox=self.periox,
                    perioy=self.perioy,
                    vel_u_adim=vel_u_adim,
                    vel_v_adim=vel_v_adim,
                )

            self.ggrad_x[field], self.ggrad_y[field] = scnd_der_2(
                self.dom.fields[field],
                self.dom.delta_x,
                periox=self.periox,
                perioy=self.perioy,
            )

    def _source_terms(self):
        """Update source_terms"""

        if self.dom.source_terms is None:
            source_force_x = np.zeros(self.dom.shape)
            source_force_y = np.zeros(self.dom.shape)
            source_scal = np.zeros(self.dom.shape)
        else:
            if self.dom.time_dependent_sources:
                sources = self.dom.source_terms(self.dm.time)
            else:
                sources = self.dom.source_terms
            source_force_x = sources["force_x"]
            source_force_y = sources["force_y"]
            source_scal = sources["scal"]

        # print("st>>>", time, source_force_x.mean(), source_force_y.mean())
        def _clean_source(src_field):
            """Make sure sources are nuffified on bounds and obstacles"""
            src_field = self.dom.nullify_on_obstacle(src_field, where="inside")

            if "wall" in self.dom.bc_xmin_type:
                src_field[:, 0] = 0.0
            if "wall" in self.dom.bc_xmax_type:
                src_field[:, -1] = 0.0
            if "wall" in self.dom.bc_ymin_type:
                src_field[0, 0] = 0.0
            if "wall" in self.dom.bc_ymax_type:
                src_field[-1, :] = 0.0

            return src_field

        _clean_source(source_force_x)
        _clean_source(source_force_y)
        _clean_source(source_scal)

        return source_force_x, source_force_y, source_scal

    def _visco(self, c_s=0.16):
        """Smagorinsky turnulent subgrid model"""

        visco_nu = np.full(self.dom.shape, self.dom.nu_ref)

        if self.smago_sgs:
            guy = self.grad_y["vel_u"]
            gux = self.grad_x["vel_u"]
            gvy = self.grad_y["vel_v"]
            gvx = self.grad_x["vel_v"]
            nu_smago = (c_s * self.dom.delta_x) ** 2 * np.sqrt(
                2.*(gux ** 2 + 0.5 * (guy +  gvx)**2 + gvy ** 2)
            )

            #print("Hey", nu_smago.max(), visco_nu.max())
            visco_nu += nu_smago

        return visco_nu

    def _apply_bnd_uv(self):
        """ Boundary conditions on velocity fields."""
        type_bc = self.dom.bc_xmin_type
        if type_bc == "periodic":
            pass  # Nothing to do
        elif type_bc == "wall_noslip":
            self.dom.fields["vel_u"][:, 0] = 0.0
            self.dom.fields["vel_v"][:, 0] = 0.0
        elif type_bc == "wall_slip":
            self.dom.fields["vel_u"][:, 0] = 0.0
        elif type_bc == "inlet":
            bnd_ux = self.dom.bc_xmin_values["vel_u"]
            self.dom.fields["vel_u"][:, 0] = bnd_ux
            self.dom.fields["vel_v"][:, 0] = 0.0
        else:
            raise NotImplementedError("X min BC type :", type_bc)

        type_bc = self.dom.bc_xmax_type
        if type_bc == "periodic":
            pass  # Nothing to do
        elif type_bc == "wall_noslip":
            self.dom.fields["vel_u"][:, -1] = 0.0
            self.dom.fields["vel_v"][:, -1] = 0.0

        elif type_bc == "wall_slip":
            self.dom.fields["vel_u"][:, -1] = 0.0
        elif type_bc == "outlet":
            pass  # Nothing to do here I guess...
        else:
            raise NotImplementedError("X max BC type :", type_bc)

        type_bc = self.dom.bc_ymin_type
        if type_bc == "periodic":
            pass  # Nothing to do
        elif type_bc == "wall_noslip":
            self.dom.fields["vel_u"][0, :] = 0.0
            self.dom.fields["vel_v"][0, :] = 0.0
        elif type_bc == "wall_slip":
            self.dom.fields["vel_v"][0, :] = 0.0
        else:
            raise NotImplementedError("Y min BC type :", type_bc)

        type_bc = self.dom.bc_ymax_type
        if type_bc == "periodic":
            pass  # Nothing to do
        elif type_bc == "wall_noslip":
            self.dom.fields["vel_u"][-1, :] = 0.0
            self.dom.fields["vel_v"][-1, :] = 0.0
        elif type_bc == "wall_slip":
            self.dom.fields["vel_v"][-1, :] = 0.0
        elif type_bc == "symmetry":
            self.dom.fields["vel_u"][-1, :] = self.dom.fields["vel_u"][-2, :]
            self.dom.fields["vel_v"][-1, :] = self.dom.fields["vel_v"][-2, :]
            self.dom.fields["vel_v"][-1, :] = 0.0
        elif type_bc == "moving_wall":
            bnd_uy = self.dom.bc_ymax_values["vel_u"]
            self.dom.fields["vel_u"][-1, :] = bnd_uy
            self.dom.fields["vel_v"][-1, :] = 0
        else:
            raise NotImplementedError("Y max BC type :", type_bc)

    def _apply_bnd_press(self, gpx, gpy):
        """Boundary conditions on pressure.

        Nullify pressure gradient on the walls"""
        type_bc = self.dom.bc_xmin_type
        if type_bc == "wall_noslip":
            gpx[0, :] = 0.0
            gpy[0, :] = 0.0
        elif type_bc == "wall_slip":
            gpx[0, :] = 0.0
            pass
        else:
            pass

        type_bc = self.dom.bc_xmax_type
        if type_bc == "wall_noslip":
            gpx[:, -1] = 0.0
            gpy[:, -1] = 0.0
        elif type_bc == "wall_slip":
            gpx[:, -1] = 0.0
        else:
            pass

        type_bc = self.dom.bc_ymin_type
        if type_bc == "wall_noslip":
            gpx[0, :] = 0.0
            gpy[0, :] = 0.0
        elif type_bc == "wall_slip":
            gpy[0, :] = 0.0
        else:
            pass

        type_bc = self.dom.bc_ymax_type
        if type_bc == "wall_noslip":
            gpx[-1, :] = 0.0
            gpy[-1, :] = 0.0
        elif type_bc == "wall_slip":
            gpy[-1, :] = 0.0
        elif type_bc == "symmetry":
            gpy[-1, :] = 0.0
        elif type_bc == "moving_wall":
            gpx[-1, :] = 0.0
            gpy[-1, :] = 0.0
        else:
            pass

        return gpx, gpy

    def _nullify_scal_grads_obs(self):
        """Nullify the scalar gradiens near obstacle"""
        self.ggrad_y["scal"] = self.dom.nullify_on_obstacle(
            self.ggrad_y["scal"], where="edges_y"
        )
        self.ggrad_x["scal"] = self.dom.nullify_on_obstacle(
            self.ggrad_x["scal"], where="edges_x"
        )
        self.grad_y["scal"] = self.dom.nullify_on_obstacle(
            self.grad_y["scal"], where="edges_y"
        )
        self.grad_x["scal"] = self.dom.nullify_on_obstacle(
            self.grad_x["scal"], where="edges_x"
        )
        # self.dom.fields["scal"] = self.dom.nullify_on_obstacle(
        #     self.dom.fields["scal"],
        #     where="inside"
        # )

    def _apply_bnd_poisson(self, ggpress):
        """Boundary conditions on pressure.

        Found on the cylinder,
        the pressure poisson on unsteady outlet must not use extrapolation.
        use "damp_outlet" boolean to enable.
        """
        if self.ctrl.damp_outlet:
            type_bc = self.dom.bc_xmax_type
            if type_bc == "outlet":
                ggpress[:, -1] = ggpress[:, -2]
        return ggpress
