Welcome to barbatruc documentation!
=========================================

Contents:

.. toctree::
   readme_copy
   howtointeractwithbarbatruc
   poiseuille_periodic
   Cavity
   cylinder
   barba_step12
   lbm_docu
   ./api/barbatruc


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

