# -*- coding: utf-8 -*-

import numpy as np
from pytest import approx

# import matplotlib.pyplot as plt

from barbatruc.fd_operators import grad_1, grad_2, scnd_der_2

ASPECT_RATIO = 3
DIM_X = 16
DIM_Y = DIM_X * ASPECT_RATIO


def test_absolute_truth_and_meaning():
    assert True


def test_grad1():
    ones_ = np.ones((DIM_Y, DIM_X))
    zeros_ = np.zeros((DIM_Y, DIM_X))
    size = 2.0 * np.pi
    x_lin = np.linspace(0, 1.0, DIM_X)
    y_lin = np.linspace(0, ASPECT_RATIO * 1.0, DIM_Y)
    dx = size / DIM_X
    cos_x = np.cos(x_lin * size)
    cos_y = np.cos(y_lin * size)
    sin_x = np.sin(x_lin * size)
    sin_y = np.sin(y_lin * size)

    test_x = cos_x[np.newaxis, :] * ones_
    test_gx = -sin_x[np.newaxis, :] * ones_
    test_ggx = -cos_x[np.newaxis, :] * ones_
    test_y = sin_y[:, np.newaxis,] * ones_
    test_gy = cos_y[:, np.newaxis,] * ones_
    test_ggy = -sin_y[:, np.newaxis,] * ones_

    grad_x, grad_y = grad_1(test_x, dx, periox=True)
    assert test_gx == approx(grad_x, abs=0.25)
    assert zeros_ == approx(grad_y)
    grad_x, grad_y = grad_1(test_x, dx)
    assert test_gx == approx(grad_x, abs=0.25)
    assert zeros_ == approx(grad_y)
    grad_x, grad_y = grad_2(test_x, dx, periox=True)
    assert test_gx == approx(grad_x, abs=0.05)
    assert zeros_ == approx(grad_y)
    grad_x, grad_y = grad_2(test_x, dx)
    assert test_gx == approx(grad_x, abs=0.25)
    assert zeros_ == approx(grad_y)

    # plt.plot(x_lin,grad_x[0, :] )
    # plt.plot(x_lin,test_gx[0,:] )
    # plt.show()

    grad_x, grad_y = grad_1(test_y, dx, perioy=True)
    assert test_gy == approx(grad_y, abs=0.25)
    assert zeros_ == approx(grad_x)
    grad_x, grad_y = grad_1(test_y, dx)
    assert test_gy == approx(grad_y, abs=0.25)
    assert zeros_ == approx(grad_x)
    grad_x, grad_y = grad_2(test_y, dx, perioy=True)
    assert test_gy == approx(grad_y, abs=0.05)
    assert zeros_ == approx(grad_x)
    grad_x, grad_y = grad_2(test_y, dx)
    assert test_gy == approx(grad_y, abs=0.25)
    assert zeros_ == approx(grad_x)

    # plt.plot(y_lin,grad_y[:,0] )
    # plt.plot(y_lin,test_gy[:,0] )
    # plt.show()

    gg_x, gg_y = scnd_der_2(test_x, dx)
    # plt.plot(x_lin,gg_x[0, :] )
    # plt.plot(x_lin,test_ggx[0,:] )
    # plt.show()
    assert test_ggx == approx(gg_x, abs=0.35)
    assert zeros_ == approx(gg_y)
    gg_x, gg_y = scnd_der_2(test_x, dx, periox=True)
    assert test_ggx == approx(gg_x, abs=0.25)
    assert zeros_ == approx(gg_y)

    gg_x, gg_y = scnd_der_2(test_y, dx)
    assert test_ggy == approx(gg_y, abs=0.35)
    assert zeros_ == approx(gg_x)
    gg_x, gg_y = scnd_der_2(test_y, dx, perioy=True)
    assert test_ggy == approx(gg_y, abs=0.25)
    assert zeros_ == approx(gg_x)
    # plt.plot(y_lin, gg_y[:, 0] )
    # plt.plot(y_lin, test_ggy[:, 0] )
    # plt.show()


# test_grad()
# test_grad2()
