barbatruc package
=================

.. automodule:: barbatruc
   :members:
   :undoc-members:
   :show-inheritance:

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   barbatruc.examples

Submodules
----------

barbatruc.barb\_orig module
---------------------------

.. automodule:: barbatruc.barb_orig
   :members:
   :undoc-members:
   :show-inheritance:

barbatruc.cli module
--------------------

.. automodule:: barbatruc.cli
   :members:
   :undoc-members:
   :show-inheritance:

barbatruc.fd\_ns\_2d module
---------------------------

.. automodule:: barbatruc.fd_ns_2d
   :members:
   :undoc-members:
   :show-inheritance:

barbatruc.fd\_operators module
------------------------------

.. automodule:: barbatruc.fd_operators
   :members:
   :undoc-members:
   :show-inheritance:

barbatruc.fluid\_domain module
------------------------------

.. automodule:: barbatruc.fluid_domain
   :members:
   :undoc-members:
   :show-inheritance:

barbatruc.lattice module
------------------------

.. automodule:: barbatruc.lattice
   :members:
   :undoc-members:
   :show-inheritance:

