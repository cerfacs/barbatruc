barbatruc.examples package
==========================

.. automodule:: barbatruc.examples
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

barbatruc.examples.cfd\_cylinder module
---------------------------------------

.. automodule:: barbatruc.examples.cfd_cylinder
   :members:
   :undoc-members:
   :show-inheritance:

barbatruc.examples.cfd\_lid\_driven module
------------------------------------------

.. automodule:: barbatruc.examples.cfd_lid_driven
   :members:
   :undoc-members:
   :show-inheritance:

barbatruc.examples.cfd\_poiseuille module
-----------------------------------------

.. automodule:: barbatruc.examples.cfd_poiseuille
   :members:
   :undoc-members:
   :show-inheritance:

