"""Example on how to solve a Poiseuille problem  with the navier stokes solver"""

import numpy as np
import matplotlib.pyplot as plt
from barbatruc.fluid_domain import DomainRectFluid
from barbatruc.fd_ns_2d import NS_fd_2D_explicit
from barbatruc.lattice import Lattice

__all__ = ["cfd_poiseuille"]


# pylint: disable=duplicate-code
def cfd_poiseuille():
    """Startup computation
    solve a volume force powered poiseuille problem
    """
    
    # Poiseuille parameters
    vel_u = 100. # m
    nu_ = 0.05 # kg/(m.s)
    width = 0.005  # m
    length = width*4
    # Volume Force computation
    dp_dx = 8. * nu_ / (width)**2. * vel_u  # Kg/(m2.s2)
    
    # Numerics
    ncell = 20 #  - Discretization
    
    # Call Daomain Creation
    dom = DomainRectFluid(
        dimx=length,
        dimy=width,
        delta_x=width/ncell,
        temp=300.0, # K
        nu_=nu_,
        rho=1.17)  # Kg/m3
    dom.switch_bc_ymax_wall_noslip()
    dom.switch_bc_ymin_wall_noslip()
    dom.fields["vel_u"] += 0.666 * vel_u # Ubulk = 2/3 Umax
    dom.set_source_terms({
        "force_x": dp_dx,
        "force_y": 0.0,
        "scal": np.zeros(dom.shape)
    })

    # Time control
    nsave = 10
    t_end = 5*width/vel_u
    time_step = t_end/nsave
    
    # Initialize solver
    #solver = Lattice(dom,pp_info=100)
    solver = NS_fd_2D_explicit(dom)

    # Temporal Loop
    for i in range(nsave):
        solver.time_advance(time_step)
        print("\n\n===============================")
        print(f"\n  Iteration {i+1}/{nsave}, Time :, {dom.time}s")
        print(f"  Reynolds : {dom.reynolds(width)}")
        print(f"  Mach : {dom.mach()}")
        
        print(dom.fields_stats())
        dom.dump_paraview()
        dom.dump_global()

    # Plot analytical curve
    n_samples = 100
    y_analytic = np.linspace(0, width, num=n_samples)
    u_analytic = np.zeros(len(y_analytic))
    for i in range(n_samples):
        u_analytic[i] = vel_u*4*(y_analytic[i]/width)*(1-(y_analytic[i]/width))
    plt.plot(y_analytic, u_analytic, color="red", marker="", label='theoretical curve')
    dom.show_profile_y()

    #solver.plot_monitor()
    #dom.show_fields()
    #dom.show_flow()
    #dom.show_debit_over_x()

    print('Normal end of execution. (^_^)')


if __name__ == "__main__":
    cfd_poiseuille()
