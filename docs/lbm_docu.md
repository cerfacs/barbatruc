# Lattice boltzmann solver documentation

The LBM solver of Barbatruc is written following two Books:

* [Mohamad, A. A. (2011). Lattice Boltzmann Method (Vol. 70). London: Springer.](https://www.springer.com/gp/book/9781447160991) : 
* [Sukop, M. (2006). DT Thorne, Jr. Lattice Boltzmann Modeling Lattice Boltzmann Modeling.](https://link.springer.com/book/10.1007%2F978-3-540-27982-2)

## General algorithm

The solver is using the D2Q9 Lattice and follows the classical approach depicted on the [Wikipedia introdution on LBM](https://en.wikipedia.org/wiki/Lattice_Boltzmann_methods).

## Inlet boundary conditions

The default inlet BC. at x_min is using the *Bounceback* approach (p77 book Mohamad, p198 book Sukop). Two other formulations are also available , with *Non equilibrium extrapolation method* (p79 book Mohamad, p194 book Sukop) and *equilibrium* (p191 book Sukop)

## Outlet boundary conditions

The default outlet BC, at x_max is using the *extrapolation* method (p79 mohamad). There is also the *density_imposed* option (p79 mohamad).

## DIfferences with the NS solver

The LBM solver share the same API as the NS solver. In particular it features a cell-blocking method using the same obstacle.

However, the current LBM implmentation has been **only tested for poiseuille flow**. Use this solver with caution for the moment
Take particular care about the conversion between physical quantities and Lattice non-dimensionned quantities.
Moreover, it **does not transport the scalar** yet. 

> Note to developers: unfortunately the axes are swapped between NS and LBM implementation. NS use X on the "axis 1" (second index of numpy array), while LBM use X on the "axis 0" (first index of numpy array). This is the origin of the following initialization
> 
```python
self.init_velocity = np.stack(
            (np.transpose(dm.fields["vel_u"]),
             np.transpose(dm.fields["vel_v"])),
            axis=0)
```