"""Definition of lattice object and its methods"""

import numpy as np
from scipy.ndimage import gaussian_filter
from copy import deepcopy

__all__ = ["Lattice"]



class Lattice(object):
    """ Lattice definition D2Q9"""

    def __init__(self, dm, verbose=False, pp_info=1000):

        print(dm)
        self.dom = dm
        self.verbose = verbose
        self.pp_info = pp_info
        self.obstacle = np.transpose(dm.obstacle)

        # init_lattice
        self._init_lattice_vars(dm.delta_x, dm.shape[1], dm.shape[0])
        self._init_lattice_struct()
        
        #  initialize the distribution function
        if self.dom.streamf is None:
            self.dom.streamf = np.zeros(self.shape) 
       
        self.dom_2_lat()
        self.compute_equilibrium_function()
        self.f_n = deepcopy(self.feq)
        self.f_tmp = deepcopy(self.feq) # for b.c.

    def _init_lattice_vars(self, delta_x, np_x, np_y):

        
        self.lu_x = delta_x
        c_s = 1.0 / np.sqrt(3.0)
        self.dim_q = 9
        self.shape = (self.dim_q, np_x, np_y)
        self.dims = np.arange(0, self.dim_q, dtype=int)
        #  = [0, 1, 2, 3, 4, 5, 6, 7, 8]

        # timestep , dmensionfull [s], limited by csound
        self.delta_t = c_s * self.lu_x / self.dom.csound

        # dimensionless viscosity
        _adim_nu = self.dom.nu_ref * self.delta_t / (self.lu_x * self.lu_x)

        # dimensionless relaxation time after implicit to explicit variables change
        self.tau = _adim_nu / (c_s * c_s) + 0.5



    def _init_lattice_struct(self):
        # D2Q9 model

        
        # The numbering of the directions in the lattice unit is like this: (see p32)
        # 6 2 5
        # 3 0 1
        # 7 4 8
        # The direction vectors in the lattice unit are relative to the center. (see p32)
        # In 2D this leads to:
        # (-1, 1)     ( 0, 1)     ( 1, 1)
        # (-1, 0)     ( 0, 0)     ( 1, 0)
        # (-1,-1)     ( 0,-1)     ( 1,-1)
        # So with the numbering the direction array is:
        # ( (0,0) (1,0) (0,1) (-1,0) (0,-1) (1,1) (-1,1) (-1,-1) (1,-1) )
        self.directions = np.zeros((self.dim_q, 2), dtype=np.int)
        self.directions[0] = [0, 0]
        self.directions[1] = [1, 0]
        self.directions[2] = [0, 1]
        self.directions[3] = [-1, 0]
        self.directions[4] = [0, -1]
        self.directions[5] = [1, 1]
        self.directions[6] = [-1, 1]
        self.directions[7] = [-1, -1]
        self.directions[8] = [1, -1]

        # weights for each direction for the equilibrium function
        # see p35 after eq17
        # ( 1/36)     ( 1/9 )     ( 1/36)
        # ( 1/9 )     ( 4/9 )     ( 1/9 )
        # ( 1/36)     ( 1/9 )     ( 1/36)
        self.weights = np.zeros(self.dim_q)
        self.weights[0] = 4.0 / 9.0
        self.weights[1] = 1.0 / 9.0
        self.weights[2] = 1.0 / 9.0
        self.weights[3] = 1.0 / 9.0
        self.weights[4] = 1.0 / 9.0
        self.weights[5] = 1.0 / 36.0
        self.weights[6] = 1.0 / 36.0
        self.weights[7] = 1.0 / 36.0
        self.weights[8] = 1.0 / 36.0

        # Bounceback boundary (see p44)
        #         6 2 5         8 4 7
        # Lattice 3 0 1 becomes 1 0 3
        #         7 4 8         5 2 6
        # self.bnd_noslip = np.zeros((self.dim_q), dtype=np.int)
        self.bnd_noslip = [0, 3, 4, 1, 2, 7, 8, 5, 6]

        # Bounceback along y and slip along x
        #         6 2 5         7 4 8
        # Lattice 3 0 1 becomes 3 0 1
        #         7 4 8         6 2 5
        # self.bnd_slip_x = np.zeros((self.dim_q), dtype=np.int)
        self.bnd_slip_x = [0, 1, 4, 3, 2, 8, 7, 6, 5]

        # Bounceback along x and slip along y
        #         6 2 5         5 2 6
        # Lattice 3 0 1 becomes 1 0 3
        #         7 4 8         8 4 7
        # self.bnd_slip_y = np.zeros((self.dim_q), dtype=np.int)
        self.bnd_slip_y = [0, 3, 2, 1, 4, 6, 5, 8, 7]

    def time_advance(self, duration):
        """Major iteration."""

        iteration = 0
        self.dom_2_lat()

        t_end = self.dom.time + duration
        
        if  self.delta_t > duration:
            self._iteration()
        else:
            while self.dom.time + self.delta_t < t_end:
                self._iteration()
                iteration += 1
                if self.verbose and iteration % self.pp_info == 0:
                    max_vel_it = np.max(self._adim_vel*self.lu_x/self.delta_t)
                    print(f"{self.dom.time}, {max_vel_it}")
  
            # One last iteration
            self._iteration()

    def _iteration(self):
        """ Perform a delta_t time advancement """

        self.dom.time += self.delta_t      
        self.compute_equilibrium_function()
        self.collision()
        self.streaming()
        self.obst_bnd()
        self.ymin_bnd()
        self.ymax_bnd()
        self.xmin_bnd()
        self.xmax_bnd()
        self.compute_density()
        self.compute_velocity()
        self.lat_2_dom() 

    def compute_velocity(self):
        """ Compute the macroscopic velocity which is an average of
            the microscopic velocities weighted by the directional
            densities (see p34 eq15)
        """
        self._adim_vel = (
            np.dot(self.directions.transpose(), self.f_n.transpose((1, 0, 2)))
            / self._adim_rho
        )
 
    def compute_density(self):
        """ Calculate macroscopic densities and velocities from the lattice units.
            The macroscopic density is the sum of direction-specific fluid densities.
            (see p33 eq14)
        """
        self._adim_rho = np.sum(self.f_n, axis=0)
 
    def compute_equilibrium_function(self):
        """ D2Q9 Equilibrium distribution function.
            see p35 eq 17 with the basic speed of the lattice
            set at c=1"""

        # Add a volume force
        # gravity

        rho = self._adim_rho
        u = self._adim_vel

        u[0, :, :] = u[0, :, :] + self.tau*self.volforces[0]
        u[1, :, :] = u[1, :, :] + self.tau*self.volforces[1]

        # Computation of ea.velocity
        ea_u = np.dot(self.directions, u.transpose(1, 0, 2))

        # Computation of velocity squared field (npx, npy)
        usqr = u[0] ** 2 + u[1] ** 2

        # equilibrium distribution function
        self.feq[self.dims, :, :] = (
                rho
                * self.weights[self.dims, None, None]
                * (1.0 + 3.0 * ea_u[self.dims] + 4.5 * ea_u[self.dims] ** 2 - 1.5 * usqr)
            )

    def streaming(self):
        """ Streaming via the distribution function.
            Direction specific densities are moved to
            the nearest neighbor lattice nodes.
            see p36
            Periodicity is applied at all the boundaries.
        """
        # This is the only space in memory needed. np.pad make a copy of
        # self.f_n, so we can now directly write to f_n below
        padded = np.pad(self.f_n, 1, mode='wrap')[1:-1]

        for i, (dx, dy) in enumerate(self.directions):
            # A trick is needed here: lastx = -1 or -2 works fine, but if it is
            # 0 it doesn't mean last element but first. None means last
            lastx, lasty = [(d if d < 0 else None) for d in (dx - 1, dy - 1)]
            self.f_n[i, :, :] = padded[i, 1+dx:lastx, 1+dy:lasty]

    def collision(self):
        """ Perform D2Q9 collision """      
        # Collision of the particles everywhere
        self.f_n = self.f_n - (self.f_n - self.feq) / self.tau

    def obst_bnd(self):
        # Collision with the Solid part => utile que si obstacle dans ecoulement
        # Bounceback boundary (see p44)
        for i_dir in range(self.dim_q):
            self.f_n[i_dir, self.obstacle] = self.f_n[
                self.bnd_noslip[i_dir], self.obstacle
            ]
        #self.f_n[:, self.obstacle] = self.f_n[self.bnd_noslip[:], self.obstacle]

    def xmin_bnd(self, inlet_method="bounceback"):
        """ Initialize the domain
        Initialize rho, u, fi, fi_eq
        """
        type_bc = self.dom.bc_xmin_type
        if type_bc == "periodic":
            pass  # Nothing to do
        elif type_bc == "wall_noslip":
            # bounce back
            # for i_dir in range(self.dim_q):
            #     self.f_tmp[i_dir, 0, :] = self.f_n[self.bnd_noslip[i_dir], 0, :]
            self.f_n[:, 0, :] = self.f_n[self.bnd_noslip[:], 0, :]
        elif type_bc == "wall_slip":
            # symmetry bounce
            # for i_dir in range(self.dim_q):
            #     self.f_tmp[i_dir, 0, :] = self.f_n[self.bnd_slip_y[i_dir], 0, :]
            self.f_n[:, 0, :] = self.f_tmp[self.bnd_slip_y[:], 0, :]
        elif type_bc == "inlet":
            bnd_ux = self.dom.bc_xmin_values["vel_u"] * self.delta_t/self.lu_x  # / self.c_vel
            self._adim_rho[0, :] = (
                self.f_n[2, 0, :]
                + self.f_n[0, 0, :]
                + self.f_n[4, 0, :]
                + 2.0 * (self.f_n[6, 0, :] + self.f_n[3, 0, :] + self.f_n[7, 0, :])
            ) / (1.0 - bnd_ux)

            if inlet_method == "non_equil":
                # Non equilibrium extrapolation method
                # p 79 book mohamad
                # p194 book sukop
                self.f_n[1, 0, :] = (
                    self.f_n[3, 0, :] + 2.0 / 3.0 * self._adim_rho[0, :] * bnd_ux
                )
                self.f_n[5, 0, :] = (
                    self.f_n[7, 0, :] + self.feq[5, 0, :] - self.feq[7, 0, :]
                )
                self.f_n[8, 0, :] = (
                    self.f_n[6, 0, :] + self.feq[8, 0, :] - self.feq[6, 0, :]
                )
            elif inlet_method == "equil":
                # Equilibrium bc inlet
                # Recompute density and a new equ
                # p191 book sukop
                self.compute_equilibrium_function()
                self.f_n[5, 0, :] = self.feq[5, 0, :]
                self.f_n[1, 0, :] = self.feq[1, 0, :]
                self.f_n[8, 0, :] = self.feq[8, 0, :]
            else:
                # Non equilibrium bounce back (default)
                # Zhu He
                # p77 book Mohamad
                # p198 book Sukop
                self.f_n[1, 0, :] = (
                    self.f_n[3, 0, :] + 2.0 / 3.0 * self._adim_rho[0, :] * bnd_ux
                )
                self.f_n[5, 0, :] = (
                    self.f_n[7, 0, :] + (1.0 / 6.0) * self._adim_rho[0, :] * bnd_ux
                )
                self.f_n[8, 0, :] = (
                    self.f_n[6, 0, :] + (1.0 / 6.0) * self._adim_rho[0, :] * bnd_ux
                )
        else:
            raise NotImplementedError("BC type :", type_bc)

        return self.f_n

    def xmax_bnd(self, outlet_method="extrapolate"):
        type_bc = self.dom.bc_xmax_type
        if type_bc == "periodic":
            pass  # Nothing to do
        elif type_bc == "wall_noslip":
            # for i_dir in range(self.dim_q):
            #     self.f_tmp[i_dir, -1, :] = self.f_n[self.bnd_noslip[i_dir], -1, :]
            self.f_n[:, -1, :] = self.f_n[self.bnd_noslip[:], -1, :]
        elif type_bc == "wall_slip":
            # for i_dir in range(self.dim_q):
            #     self.f_tmp[i_dir, -1, :] = self.f_n[self.bnd_slip_y[i_dir], -1, :]
            self.f_n[:, -1, :] = self.f_tmp[self.bnd_slip_y[:], -1, :]
        elif type_bc == "outlet":
            rho_outlet = self.dom.rho_ref
            if outlet_method == "density_imposed":
                # Open imposed density boundary
                # p79 mohamad
                self._adim_vel[0, -1, :] = (
                    self.f_n[2, -1, :]
                    + self.f_n[0, -1, :]
                    + self.f_n[4, -1, :]
                    + 2.0
                    * (self.f_n[5, -1, :] + self.f_n[1, -1, :] + self.f_n[8, -1, :])
                ) / rho_outlet - 1.0

                rhoux_out = rho_outlet * self._adim_vel[0, -1, :]
                self.f_n[6, -1, :] = self.f_n[8, -1, :] - 1.0 / 6.0 * rhoux_out
                self.f_n[3, -1, :] = self.f_n[1, -1, :] - 2.0 / 3.0 * rhoux_out
                self.f_n[7, -1, :] = self.f_n[5, -1, :] - 1.0 / 6.0 * rhoux_out
            else:
                # Simple extrapolation at the outlet
                # p79 mohamad
                self.f_n[6, -1, :] = 2.0 * self.f_n[6, -2, :] - self.f_n[6, -3, :]
                self.f_n[3, -1, :] = 2.0 * self.f_n[3, -2, :] - self.f_n[3, -3, :]
                self.f_n[7, -1, :] = 2.0 * self.f_n[7, -2, :] - self.f_n[7, -3, :]
        else:
            raise NotImplementedError("BC type :", type_bc)

        return self.f_n

    def ymin_bnd(self):
        type_bc = self.dom.bc_ymin_type
        if type_bc == "periodic":
            pass  # Nothing to do
        elif type_bc == "wall_noslip":
            # for i_dir in range(self.dim_q):
            #     self.f_tmp[i_dir, :, 0] = self.f_n[self.bnd_noslip[i_dir], :, 0]
            self.f_n[:, :, 0] = self.f_n[self.bnd_noslip[:], :, 0]

        elif type_bc == "wall_slip":
            # for i_dir in range(self.dim_q):
            #     self.f_tmp[i_dir, :, 0] = self.f_n[self.bnd_slip_x[i_dir], :, 0]
            self.f_n[:, :, 0] = self.f_tmp[self.bnd_slip_x[:], :, 0]
        else:
            raise NotImplementedError("BC type :", type_bc)

        return self.f_n

    def ymax_bnd(self):
        type_bc = self.dom.bc_ymax_type
        if type_bc == "periodic":
            pass  # Nothing to do
        elif type_bc == "wall_noslip":
            # for i_dir in range(self.dim_q):
            #     self.f_tmp[i_dir, :, -1] = self.f_n[self.bnd_noslip[i_dir], :, -1]
            self.f_n[:, :, -1] = self.f_n[self.bnd_noslip[:], :, -1]
        elif type_bc == "wall_slip":
            # for i_dir in range(self.dim_q):
            #     self.f_tmp[i_dir, :, -1] = self.f_n[self.bnd_slip_x[i_dir], :, -1]
            self.f_n[:, :, -1] = self.f_tmp[self.bnd_slip_x[:], :, -1]
        else:
            raise NotImplementedError("BC type :", type_bc)

        return self.f_n

    def filter(self):
        #  Filtering
        self._adim_rho = gaussian_filter(self._adim_rho, 0.1)
        self._adim_vel[0, :, :] = gaussian_filter(self._adim_vel[0, :, :], 0.5)
        self._adim_vel[1, :, :] = gaussian_filter(self._adim_vel[1, :, :], 0.5)

    def lat_2_dom(self):
        # update to domain the content of lattice result
        dim_vel = self._adim_vel * self.lu_x/self.delta_t
        self.dom.fields["vel_u"] = dim_vel[0, :, :].T
        self.dom.fields["vel_v"] = dim_vel[1, :, :].T
        self.dom.fields["press"] = self._adim_rho.T * self.dom.p_ref
    
    def dom_2_lat(self):
        # update to domain the content of lattice result
        dim_vel = np.stack(
            (np.transpose(self.dom.fields["vel_u"]), np.transpose(self.dom.fields["vel_v"])), axis=0
        )
        self._adim_vel = dim_vel * self.delta_t/self.lu_x
        self._adim_rho=self.dom.fields["press"].T / (self.dom.p_ref)
        self.feq = self.dom.streamf
        
        self.volforces = np.zeros((2))
        if self.dom.source_terms is not None:
            self.volforces[0] = self.dom.source_terms["force_x"]
            self.volforces[1] = self.dom.source_terms["force_y"]
        else:
            self.volforces[0] = 0.
            self.volforces[1] = 0.

        self.volforces = self.volforces*(self.delta_t*self.delta_t/self.lu_x)



# For convenience of treating the boundary conditions, keep in a table the indicies
# of the dimensions relative to the left, middle and right column (axis y)
# and also the down, middle and up line (axis x)
# self.bnd_i1 = np.zeros(3, dtype=int)  # First column (left bnd)
# self.bnd_i1[0] = 3
# self.bnd_i1[1] = 6
# self.bnd_i1[2] = 7
# self.bnd_i2 = np.zeros(3, dtype=int)  # Second column (middle vertical)
# self.bnd_i2[0] = 0
# self.bnd_i2[1] = 2
# self.bnd_i2[2] = 4
# self.bnd_i3 = np.zeros(3, dtype=int)  # Third column (right bnd)
# self.bnd_i3[0] = 1
# self.bnd_i3[1] = 5
# self.bnd_i3[2] = 8
# self.bnd_j1 = np.zeros(3, dtype=int)  # First line (down bnd)
# self.bnd_j1[0] = 4
# self.bnd_j1[1] = 7
# self.bnd_j1[2] = 8
# self.bnd_j2 = np.zeros(3, dtype=int)  # Second line (middle horizontal)
# self.bnd_j2[0] = 0
# self.bnd_j2[1] = 1
# self.bnd_j2[2] = 3
# self.bnd_j3 = np.zeros(3, dtype=int)  # Third line (up bnd)
# self.bnd_j3[0] = 2
# self.bnd_j3[1] = 5
# self.bnd_j3[2] = 6
