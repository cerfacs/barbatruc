# Welcome to Barbatruc


## The BARBATRUC project

BARBATRUC is a computationnal fluid dynamics (CFD) package dedicated to training. The target audience is people starting CFD. The (tentative) objectives are:

* **Physics**: Toy with basic CFD configurations (Poiseuille, Lid-driven cavity, Karman-street) with a light-weight solver. 
* **Scientific computing**: Tinker several solvers for CFD, a Navier-Stockes finite difference incompressible solver and a Lattice Boltzmann solver.
* **Software development**: See how two solvers show the same interface, by messing around with the post-processing of the data-structure regardless of the solver. Explore the continuous integration, linting, testing and automatic documentation featured by the package.


## Installation

Barbatruc is available on Python Package Index

```
pip install barbatruc
```

You might also want to make a git clone from the [barbatruc gitlab.com repository](https://gitlab.com/cerfacs/barbatruc) (under creation)

```
git clone git@gitlab.com:cerfacs/barbatruc.git
```

> Note : The gitlab.com repository is read only. It is a mirror of the actual repository hosted privately at [Cerfacs Forge](https://nitrox.cerfacs.fr/open-source/barbatruc)

## Usage

Type `barbatruc` and you will gat the CLI menu.

```bash
Usage: barbatruc [OPTIONS] COMMAND [ARGS]...

  ---------------   BARBATRUC  --------------------

  You are now using the Command line interface of BARBATRUC, a Python3
  training package on CFD , created at CERFACS (https://cerfacs.fr).

  This is a python package currently installed in your python environement.

Options:
  --help  Show this message and exit.

Commands:
  create  Wizard creating a BARBATRUC script NAME.py.
  docu    Open Barbatruc documentation
```

Create a new project with the `create`option.

```bash
>barbatruc create barbibul cfd_poiseuille 
```

This will create a local copy of the Poiseuille example.
Now you can edit the newly created script `barbibul.py` and run it with

```bash
>python barbibul.py 
```


> Note : Soon the software will be able to say "barbatruc" when you interact with it. As this feature is missing now, please say "barbatruc" whenever you type a command or start a run.

## An example:

A typical barbatruc script looks like:


```python
import numpy as np
from barbatruc.fluid_domain import DomainRectFluid
from barbatruc.fd_ns_2d import NS_fd_2D_explicit

def cfd_lid_driven(nsave, t_end):
    """Startup computation
    solve a lid_driven_cavity  problem
    """
    dom = DomainRectFluid(dimx=0.82, dimy=0.61, delta_x=0.02)
    vel = 0.0001
    dom.switch_bc_xmin_wall_noslip()
    dom.switch_bc_xmax_wall_noslip()
    dom.switch_bc_ymax_moving_wall(vel_u=vel)
    dom.switch_bc_ymin_wall_noslip()
    time = 0.0
    time_step = t_end/nsave
    solver = NS_fd_2D_explicit(dom, obs_ib_factor=0.9)
    for i in range(nsave):
        time += time_step
        solver.iteration(time, time_step)
        print('  Max u:', np.max(dom.fields["vel_u"]))
        print('  Time :', time)
        print('  Iteration %d' % (i))
    dom.show_fields()
    dom.show_flow()
    print('Normal end of execution.')

if __name__ == "__main__":
    cfd_lid_driven(10, 0.50)
```

## Why this weird name?

 Barbapapa is a [1970 childern picture book](https://en.wikipedia.org/wiki/Barbapapa) where the character can morph into anything while saying "Barba-trick", in french "Barbatruc".
The Navier Stokes solver stems from Pr. Lorena Barba's 12 steps to Naviers Stokes course. Initially a running gag between co-workers, `barbatruc` eventually became the name of the package and landed on PyPI.

> Note to Pr. Barba, if you are reading this. All apologies for this pun, you are probably totally sick of it. Many thanks for making available to all your 12-steps-to-NS course.
