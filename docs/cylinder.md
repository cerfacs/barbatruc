# Cylinder Flow - Von Karman Street

## Configuration

We want to have a flow around a cylinder with :

**Re = 100**

    lenght = 1.0
    vel = 1.0
    diameter = 0.05
    t_end = 4.0 * lenght / vel
    dom = DomainRectFluid(dimx=lenght, dimy=0.5 * lenght, delta_x=0.005, nu_=0.0005)


## Reference case

### the script	

```python
			"""Example on how to solve a Karmann Street problem with the navier stokes solver"""
	
	from barbatruc.fluid_domain import DomainRectFluid
	from barbatruc.fd_ns_2d import NS_fd_2D_explicit
	
	# from barbatruc.lattice import Lattice
	
	__all__ = ["cfd_cylinder"]
	
	
	# pylint: disable=duplicate-code
	def cfd_cylinder(nsave):
	    """Startup computation
	    solve a cylinder obstacle problem
	    """
	    lenght = 1.0
	    vel = 1.0
	    diameter = 0.05
	    t_end = 4.0 * lenght / vel
	    dom = DomainRectFluid(dimx=lenght, dimy=0.5 * lenght, delta_x=0.005, nu_=0.0005)
	    dom.add_obstacle_circle(x_c=0.2 * lenght, radius=0.5 * diameter)
	    dom.switch_bc_xmin_inlet(vel_u=vel)
	    dom.switch_bc_xmax_outlet()
	    dom.fields["vel_u"] += vel
	    dom.fields["vel_v"] += 0.1 * vel

	    print(dom)
	
	    time = 0.0
	    time_step = t_end / nsave
	
	    # solver_lbm = Lattice(dom, max_vel=2*vel)
	    solver = NS_fd_2D_explicit(
	        dom,
	        max_vel=2.0 * vel,
	        obs_ib_factor=0.4,
	        obs_ib_integral=False,
	        # obs_ib_factor=0.001,
	        press_maxsteps=200,
	        damp_outlet=True,
	    )
	
	    for i in range(nsave):
	        solver.iteration(time, time_step)
	        time += time_step
	        print("\n\n===============================")
	        print(f"\n  Iteration {i+1}/{nsave}, Time :, {time}s")
	        print(f"  Reynolds : {dom.reynolds(diameter)}")
	        print(dom.fields_stats())
	        dom.dump_paraview(time=time)
	        dom.dump_global(time=time)
	    dom.show_fields()
	    dom.show_flow()
	    dom.show_profile_y(xtgt=0.5)
	
	    print("Normal end of execution.")


	if __name__ == "__main__":
	    cfd_cylinder(200)
```python

### The flow output :
![karmancylinderflow](cylinder/flow_cyl_100.png)

### The fields :
![karmancylinderfields](cylinder/fields_cyl_100.png) 

### The velocity profile :
![karmancylinderprofile](cylinder/profile_cyl_100.png) 
  

### Global parameters:
![global](cylinder/global_cylinder.png)

### Drag force:
![frag](cylinder/f_x.png)

Thanks to this graph, we got the experimental drag coefficient : **C_x = 1.43**. The theoretical one is **C_x=1.3**

The drag Force is about **0.04 N/m**.

### Lift force:
![lift](cylinder/f_y.png)

Thanks to this graph, we got the vortex shedding frequency : **f = 2.5773 Hz**, which leads to a Strouhal number: **St = 0.128865**, the theoretical one is **St=0.16**

### Solver parameters:
![global](cylinder/solver_cylinder.png)