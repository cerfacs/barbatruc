"""
 Finite differences operators
 ============================

 """

import numpy as np

__all__ = ["grad_1", "grad_2", "scnd_der_2"]

# pylint: disable=unsupported-assignment-operation,invalid-sequence-index


def grad_1(array_, d_x, periox=False, perioy=False, vel_u_adim=None, vel_v_adim=None):
    """Compute the two gradients of an array."""

    if len(array_.shape) == 3:
        if vel_u_adim is not None:
            vel_u_adim = vel_u_adim[:, :, np.newaxis]
        if vel_v_adim is not None:
            vel_v_adim = vel_v_adim[:, :, np.newaxis]

    g_x = grad_1st_order(array_, d_x, perio=periox, vel_adim=vel_u_adim)
    if vel_v_adim is not None:

        vel_v_adim = np.swapaxes(vel_v_adim, 0, 1)
    
    g_y = grad_1st_order(
        np.swapaxes(array_, 0, 1), d_x, perio=perioy, vel_adim=vel_v_adim)
    g_y = np.swapaxes(g_y, 0, 1)
    return g_x, g_y


def grad_2(array_, d_x, periox=False, perioy=False):
    """Compute the two gradients of an array."""
    g_x = grad_2nd_order(array_, d_x, perio=periox)
    g_y = grad_2nd_order(np.swapaxes(array_, 0, 1), d_x, perio=perioy)
    g_y = np.swapaxes(g_y, 0, 1)
    return g_x, g_y


def scnd_der_2(array_, d_x, periox=False, perioy=False, no_center=False):
    """Compute the two scnd derivatives of an array."""
    gg_x = gradgrad_2nd_order(array_, d_x, perio=periox, no_center=no_center)
    gg_y = gradgrad_2nd_order(
        np.swapaxes(array_, 0, 1), d_x, perio=perioy, no_center=no_center
    )
    gg_y = np.swapaxes(gg_y, 0, 1)

    return gg_x, gg_y


def grad_1st_order(array_, d_x, perio=True, vel_adim=None):
    """Compute gradient of array (1st order backward)

    Apply only on axis 1 """
    grad_f = np.ones_like(array_)
    grad_b = np.ones_like(array_)

    grad_f[:, 1:-1] = (array_[:, 1:-1] - array_[:, 0:-2]) / d_x
    if perio:
        grad_f[:, -1] = (array_[:, -1] - array_[:, -2]) / d_x
        grad_f[:, 0] = (array_[:, 0] - array_[:, -2]) / d_x
    else:
        grad_f[:, -1] = (array_[:, -1] - array_[:, -2]) / d_x
        grad_f[:, 0] = (array_[:, 1] - array_[:, 0]) / d_x

    grad_b[:, 1:-1] = (array_[:, 2:] - array_[:, 1:-1]) / d_x
    if perio:
        grad_b[:, -1] = (array_[:, 1] - array_[:, -1]) / d_x
        grad_b[:, 0] = (array_[:, 1] - array_[:, -1]) / d_x
    else:
        grad_b[:, -1] = (array_[:, -1] - array_[:, -2]) / d_x
        grad_b[:, 0] = (array_[:, 1] - array_[:, 0]) / d_x

    if vel_adim is None:
        vel_adim = 1.0
    else:
        vel_adim = 0.5 * (np.clip(10.0 * vel_adim, -1.0, 1.0) + 1.0)

    return vel_adim * grad_f + (1.0 - vel_adim) * grad_b


def grad_2nd_order(array_, d_x, perio=True):
    """Compute gradient of array (2nd order centered)

    Apply only on axis 1
    """
    grad_ = np.zeros_like(array_)
    grad_[:, 1:-1] = (array_[:, 2:] - array_[:, 0:-2]) / (2 * d_x)

    if perio:
        grad_[:, -1] = (array_[:, 1] - array_[:, -2]) / (2 * d_x)
        grad_[:, 0] = (array_[:, 1] - array_[:, -2]) / (2 * d_x)
    else:
        grad_[:, -1] = (array_[:, -1] - array_[:, -2]) / d_x
        grad_[:, 0] = (array_[:, 1] - array_[:, 0]) / d_x

    # grad_[:, 1:-1] = (
    #     0.5*grad_[:, 1:-1]
    #     + 0.25*grad_[:, :-2]
    #     + 0.25*grad_[:, 2:]
    # )
    return grad_


def gradgrad_2nd_order(array_, d_x, perio=True, no_center=False):
    """Compute gradient of array (2nd order centered)

    Extrapolate is no periodicity given."""
    ggrad = np.zeros_like(array_)
    if no_center:
        coef_center = 0
    else:
        coef_center = 2
    ggrad[:, 1:-1] = (
        array_[:, 2:] - coef_center * array_[:, 1:-1] + array_[:, 0:-2]
    ) / d_x ** 2

    if perio:
        ggrad[:, -1] = (
            array_[:, 1] - coef_center * array_[:, -1] + array_[:, -2]
        ) / d_x ** 2
        ggrad[:, 0] = (
            array_[:, 1] - coef_center * array_[:, 0] + array_[:, -2]
        ) / d_x ** 2
    else:
        ggrad[:, -1] = 2 * ggrad[:, -2] - ggrad[:, -3]
        ggrad[:, 0] = 2 * ggrad[:, 1] - ggrad[:, 2]

    # ggrad[:, 1:-1] = (
    #     0.5*ggrad[:, 1:-1]
    #     + 0.25*ggrad[:, :-2]
    #     + 0.25*ggrad[:, 2:]
    # )

    return ggrad
